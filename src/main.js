import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App";
import "normalize.css";
import axios from "axios";
import Vuelidate from "vuelidate";

axios.defaults.baseURL = "http://127.0.0.1:8000/";

const app = createApp(App);
app.use(Vuelidate);
app.use(ElementPlus);
app.mount("#app");
